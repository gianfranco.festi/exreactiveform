import { Component } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'exReactiveForm';
  frmProdotto: FormGroup;
  constructor(fb:FormBuilder){
    this.frmProdotto=fb.group({
        id: [1] //new FormControl('1') // tanto per provare
    })
  }
  
  onSubmit(){
    console.log(this.frmProdotto.value);

  }
 
}
